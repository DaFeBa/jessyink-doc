# jessyink-doc
Documentations and templates for Jessyink for Inkscape

Created by Darío Badagnani

``` /docs ```
 Contains the Spanish and English manuals

``` /Componets ```
Buttons and designs

``` /Templates ```
 Slides with design and font linked
 
``` /Examples ```
Presentations made using Jessyink